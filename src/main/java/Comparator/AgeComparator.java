package Comparator;

import Person.Person;

public class AgeComparator implements Comparator {
    public int compare(Person p1, Person p2) {
        return p1.getAge().compareTo(p2.getAge());
    }
}
