package Comparator;

import Person.Person;

public interface Comparator {
    int compare(Person p1, Person p2);
}
