package Container;

import Comparator.Comparator;
import EqualChecker.EqualChecker;
import Injector.AutoInjectable;
import Person.Person;
import Sorter.Sorter;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

public class Container {
    private final static Logger log = Logger.getLogger(Container.class);
    private Person[] people;
    private int pointer;
    @AutoInjectable
    Sorter sorter;

    public Container() {
        people = new Person[2];
        pointer = 0;
    }

    /**
     * Adds a person to repository
     * @param person - object to be added
     */
    public void add(Person person) {
        log.info("Adding: " + person);
        log.info("Before: " + (pointer - 1));
        person.setID(pointer + 1);
        if (pointer == people.length) {
            Person[] newPeople = new Person[people.length + 2];
            System.arraycopy(people, 0, newPeople, 0, people.length);
            people = newPeople;
        }
        people[pointer] = person;
        pointer++;
        log.info("After: " + (pointer - 1));
    }

    /**
     * Finds a person
     * @param query what we need to find
     * @param equalChecker - instructions on what exactly to compare
     * @return if a person is found, return them, else null
     */
    public Person find(Object query, EqualChecker equalChecker) {
        log.info("Started the search, query - " + query);
        for (int i = 0; i < pointer; i++)
            if (equalChecker.isEqual(people[i], query)) {
                log.info("Found " + people[i]);
                return people[i];
            }
        return null;
    }

    /**
     * Deletes a person
     * @param query - what parameter to use to find a person
     * @param equalChecker instructions on what exactly to compare
     */
    public void delete(Object query, EqualChecker equalChecker) {
        try {
            log.info("Deleting: " + query);
            log.info("Before: " + (pointer - 1));
            boolean found = false;
            for (int i = 0; i < pointer - 1; i++) {
                if (equalChecker.isEqual(people[i], query)) {
                    log.info("Found");
                    found = true;
                }
                if (found)
                    people[i] = people[i + 1];
            }
            pointer--;
            log.info("After: " + (pointer - 1));
        }
        catch (Exception e) {
            log.error("Exception occurred: ", e);
        }
    }

    public int getLength() {
         return pointer;
    }

    public Person getByIndex(int i) {
        return people[i];
    }

    public Sorter getSorter() {
        return sorter;
    }

    /**
     * Sorts array of people
     * @param sorter - sorting algorithm to be used
     * @param comparator - instructions on what exactly to compare
     */
    public void sort(Sorter sorter, Comparator comparator) {
        sorter.sort(people, comparator);
    }
    public void sort(Comparator comparator) {
        sorter.sort(people, comparator);
    }
}
