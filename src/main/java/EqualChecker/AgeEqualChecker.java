package EqualChecker;

import Person.Person;

public class AgeEqualChecker implements EqualChecker {
    public boolean isEqual(Person p, Object o) {
        return p.getAge().equals(o);
    }
}