package EqualChecker;

import Person.Person;

public interface EqualChecker {
    boolean isEqual(Person p, Object o);
}
