package EqualChecker;

import Person.Person;

public class FIOEqualChecker implements EqualChecker {
    public boolean isEqual(Person p, Object o) {
        return p.getName().equals(o);
    }
}
