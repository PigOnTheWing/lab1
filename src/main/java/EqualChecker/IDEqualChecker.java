package EqualChecker;

import Person.Person;

public class IDEqualChecker implements EqualChecker {
    public boolean isEqual(Person p, Object o) {
        return p.getID().equals(o);
    }
}