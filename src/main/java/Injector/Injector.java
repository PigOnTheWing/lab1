package Injector;

import Sorter.Sorter;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Properties;

public class Injector {
    private String sorterConfig;
    public Injector() {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream(new File("/home/smol/IdeaProjects/Lab1/src/main/resources/Sorter.properties")));
            sorterConfig = props.getProperty("Sorter");
        }
        catch (IOException e) {
            System.out.println("Not found");
        }
    }

    public void Inject(Object queuedForInjection) {
        Field[] fields = queuedForInjection.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.getAnnotation(AutoInjectable.class) != null) {
                try {
                    Class sorter = Class.forName(sorterConfig);
                    Sorter instanceSorter = (Sorter) sorter.newInstance();
                    field.setAccessible(true);
                    field.set(queuedForInjection, instanceSorter);
                }
                catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    System.out.println("Config is corrupted");
                }
            }
        }
    }
}
