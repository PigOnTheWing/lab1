package Main;

import Container.Container;
import EqualChecker.FIOEqualChecker;
import Person.Person;
import org.joda.time.DateTime;

public class Main {
    public static void main(String[] args) {
        Container container = new Container();
        container.add(new Person("3", true, new DateTime("1996-12-21")));
        container.add(new Person("3", true, new DateTime("1996-12-20")));
        container.add(new Person("5", false, new DateTime("1990-03-11")));
        container.add(new Person("1", true, new DateTime("1995-11-21")));
        container.add(new Person("4", false, new DateTime("1999-11-21")));
        container.add(new Person("2", false, new DateTime("1973-06-01")));
        container.delete("1", new FIOEqualChecker());
    }
}
