package Person;

import org.joda.time.DateTime;

public class Person {
    private Integer ID;
    private String name;
    private boolean gender;
    private DateTime birthDate;

    public String getName() {
        return name;
    }
    public void setName(String name) { this.name = name; }

    public String getGender() {
        if (gender)
            return "Male";
        else
            return "Female";
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(DateTime birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getAge() {
        DateTime now = DateTime.now();
        int afterBirthdayAge = now.getYear() - birthDate.getYear();
        if ((now.getMonthOfYear() == birthDate.getMonthOfYear() &&
                now.getDayOfMonth() >= birthDate.getDayOfMonth()) || now.getMonthOfYear() > birthDate.getMonthOfYear()) {
            return afterBirthdayAge;
        }
        return afterBirthdayAge - 1;
    }

    public Integer getID() { return ID; }
    public void setID(int newID) { this.ID = newID; }

    public Person() { }

    public Person(String name, boolean gender, DateTime birthDate) {
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Name: " + getName() + "\nAge: " + getAge() + "\nGender: " + getGender();
    }
}
