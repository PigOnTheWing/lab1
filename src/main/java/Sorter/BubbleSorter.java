package Sorter;

import Comparator.Comparator;
import Person.Person;

public class BubbleSorter implements Sorter {
    public void sort(Person[] target, Comparator comparator) {
        for (int i = 0; i < target.length; i++)
            for (int j = 0; j < target.length - i - 1; j++)
                if (comparator.compare(target[j], target[j + 1]) > 0){
                    Person tmp = target[j];
                    target[j] = target[j + 1];
                    target[j + 1] = tmp;
                }
    }
}
