package Sorter;

import Comparator.Comparator;
import Person.Person;

public class InsertionSorter implements Sorter {
    public void sort(Person[] target, Comparator comparator) {
        for (int i = 1; i < target.length; i++) {
            Person key = target[i];
            int j = i - 1;
            while (j >= 0 && comparator.compare(target[j], key) > 0){
                target[j + 1] = target[j];
                j--;
            }
            target[j + 1] = key;
        }
    }
}
