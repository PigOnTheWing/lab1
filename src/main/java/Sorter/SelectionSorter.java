package Sorter;

import Comparator.Comparator;
import Person.Person;

public class SelectionSorter implements Sorter {
    public void sort(Person[] target, Comparator comparator) {
        for (int i = 0; i < target.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < target.length; j++)
                if (comparator.compare(target[j], target[minIndex]) < 0)
                    minIndex = j;

            Person tmp = target[minIndex];
            target[minIndex] = target[i];
            target[i] = tmp;
        }
    }
}
