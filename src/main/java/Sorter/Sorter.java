package Sorter;

import Comparator.Comparator;
import Person.Person;

public interface Sorter {
    void sort(Person[] target, Comparator comparator);
}
