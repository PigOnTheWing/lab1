import Container.Container;
import EqualChecker.EqualChecker;
import Person.Person;
import EqualChecker.*;
import org.joda.time.DateTime;
import org.junit.jupiter.api.*;
import java.util.concurrent.ThreadLocalRandom;

class ContainerTester {
    private Person testSubject;
    private EqualChecker checker;
    private Container container;

    @BeforeEach
    void init() {
        try {
            Class cont = Class.forName(Container.class.getName());
            container = (Container) cont.newInstance();
            container.add(new Person("3", true, new DateTime("1996-12-20")));
            container.add(new Person("5", false, new DateTime("1990-03-11")));
            testSubject = new Person("Name", true, new DateTime("1234-12-12"));
        }
        catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        int a = ThreadLocalRandom.current().nextInt(0, 3);
        switch (a) {
            case 0: {
                checker = new FIOEqualChecker();
                break;
            }
            case 1: {
                checker = new AgeEqualChecker();
                break;
            }
            case 2: {
                checker = new IDEqualChecker();
                break;
            }
        }
    }

    @Test
    void additionDeletionTest() {
        int length = container.getLength();
        Person tst;
        container.add(testSubject);
        Assertions.assertEquals(length + 1, container.getLength());
        if (checker.getClass().getName().equals("EqualChecker.FIOEqualChecker")) {
            tst = container.find(testSubject.getName(), checker);
            Assertions.assertEquals(tst, testSubject);
            container.delete(testSubject.getName(), checker);
        }
        else if (checker.getClass().getName().equals("EqualChecker.AgeEqualChecker")) {
            tst = container.find(testSubject.getAge(), checker);
            Assertions.assertEquals(tst, testSubject);
            container.delete(testSubject.getAge(), checker);
        }
        else {
            tst = container.find(testSubject.getID(), checker);
            Assertions.assertEquals(tst, testSubject);
            container.delete(testSubject.getID(), checker);
        }
        Assertions.assertEquals(length, container.getLength());
    }
}