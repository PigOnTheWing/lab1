import Comparator.*;
import Container.Container;
import Injector.Injector;
import Person.Person;
import Sorter.*;
import org.joda.time.DateTime;
import org.junit.jupiter.api.*;

import java.util.concurrent.ThreadLocalRandom;

class SortingTester {

    private Container container;
    private Sorter sorter;
    private Comparator comparator;

    @BeforeEach
    void init() {
        container = new Container();
        container.add(new Person("3", true, new DateTime("1996-12-21")));
        container.add(new Person("3", true, new DateTime("1996-12-20")));
        container.add(new Person("5", false, new DateTime("1990-03-11")));
        container.add(new Person("1", true, new DateTime("1995-11-21")));
        container.add(new Person("4", false, new DateTime("1999-11-21")));
        container.add(new Person("2", false, new DateTime("1973-06-01")));

        int a = ThreadLocalRandom.current().nextInt(0, 3);
        switch (a) {
            case 0: {
                sorter = new BubbleSorter();
                break;
            }
            case 1: {
                sorter = new InsertionSorter();
                break;
            }
            case 2: {
                sorter = new SelectionSorter();
                break;
            }
        }
        int b = ThreadLocalRandom.current().nextInt(0, 3);
        switch (b) {
            case 0: {
                comparator = new FIOComparator();
                break;
            }
            case 1: {
                comparator = new AgeComparator();
                break;
            }
            case 2: {
                comparator = (Person p1, Person p2) -> p2.getBirthDate().compareTo(p1.getBirthDate());
                break;
            }
        }
    }

    @RepeatedTest(100)
    void SorterTest() {
        container.sort(sorter, comparator);
        Assertions.assertEquals(6, container.getLength());
        for (int i = 0; i < container.getLength() - 1; i++) {
            Person p1 = container.getByIndex(i);
            Person p2 = container.getByIndex(i + 1);
            Assertions.assertAll("successful search",
                    () -> Assertions.assertNotNull(p1),
                    () -> Assertions.assertNotNull(p2)
            );

            Assertions.assertTrue(comparator.compare(p1, p2) <= 0);
        }
    }

    @Test
    void InjectionTest() {
        Injector injector = new Injector();
        injector.Inject(container);
        Assertions.assertEquals(InsertionSorter.class, container.getSorter().getClass());
    }

    @AfterEach
    void Clear() {
        container = null;
        comparator = null;
        sorter = null;
    }
}